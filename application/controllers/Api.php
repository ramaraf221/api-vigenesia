<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

date_default_timezone_set('Asia/Jakarta');
class Api extends RestController
{

    /*
    Link Api Authentifikasi Zone Star
    */
    public function user_get($id = 0)
    {
        $data = $this->db->get_where('user', ['iduser' => $id])->result_array();
        if ($id) {
            if ($data) {
                $this->response($data, RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'data tidak ditemukan',
                ], RestController::HTTP_NOT_FOUND);
            }
        } else {
            $data = $this->db->get('user')->result();
            $this->response($data, RestController::HTTP_OK);
        }
    }

    public function user_post()
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'profesi' => $this->input->post('profesi'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'role_id' => 2,
            'is_active' => 1,
            'tanggal_input' => date('Y-m-d'),
            'modified' => date('Y-m-d'),
        );

        $post = $this->db->insert('user', $data);
        if ($post) {
            $this->response([
                'status' => 'success',
                'message' => 'data ditambah',
            ], RestController::HTTP_OK);
        } else {
            $this->response([
                'status' => 'failed',
            ], RestController::HTTP_INTERNAL_ERROR);
        }
    }

    public function user_put()
    {
        $iduser = $this->put('iduser');
        $data = array(
            'nama' => $this->put('nama'),
            'profesi' => $this->put('profesi'),
            'email' => $this->put('email'),
            'password' => $this->put('password'),
            'role_id' => 2,
            'is_active' => 1,
            'tanggal_input' => date('Y-m-d'),
            'modified' => date('Y-m-d'),
        );

        $this->db->where('iduser', $iduser);
        $update = $this->db->update('user', $data);
        if ($update) {
            $this->response([
                'status' => 'success',
                'message' => 'data diupdate',
            ], RestController::HTTP_OK);
        } else {
            $this->response([
                'status' => 'failed',
            ], RestController::HTTP_INTERNAL_ERROR);
        }
    }

    public function login_post()
    {
        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        if (empty($email)) {
            $this->response([
                'status' => false,
                'message' => 'email fill required',
            ], RestController::HTTP_INTERNAL_ERROR);
        } elseif (empty($pass)) {
            $this->response([
                'status' => false,
                'message' => 'password fill required',
            ], RestController::HTTP_INTERNAL_ERROR);
        } else {
            $user = $this->db->get_where('user', ['email' => $email])->row_array();
            if ($user) {
                if ($user['password'] == $pass) {
                    $this->response([
                        'status' => 'success',
                        'message' => 'berhasil login',
                    ], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'password incorrect',
                    ], RestController::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'email do not match',
                ], RestController::HTTP_NOT_FOUND);
            }
        }
    }
    /*
    Link Api Authentifikasi Zone End
    */


    public function POSTmotivasi_post()
    {
        $data = array(
            'isi_motivasi' => $this->input->post('isi_motivasi'),
            'iduser'    => $this->input->post('iduser'),
            'tanggal_input' => date('Y-m-d'),
            'tanggal_update' => date('Y-m-d'),
        );

        $post = $this->db->insert('motivasi', $data);
        if ($post) {
            $this->response([
                'status' => 'success',
                'message' => 'data ditambah',
            ], RestController::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'failed',
            ], RestController::HTTP_INTERNAL_ERROR);
        }
    }

    public function PUTmotivasi_put()
    {
        $id = $this->put('id');
        $data = array(
            'isi_motivasi' => $this->put('isi_motivasi'),
            'iduser'    => $this->put('iduser'),
            'tanggal_update' => date('Y-m-d'),
        );

        $this->db->where('id', $id);
        $put = $this->db->update('motivasi', $data);
        if ($put) {
            $this->response([
                'status' => 'success',
                'message' => 'data diupdate',
            ], RestController::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'failed',
            ], RestController::HTTP_INTERNAL_ERROR);
        }
    }

    public function Get_motivasi_get()
    {
        $data = $this->db->get('motivasi')->result();
        $this->response($data, RestController::HTTP_OK);
    }

    public function DELETEmotivasi_delete()
    {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $data = $this->db->delete('motivasi');
        if ($data) {
            $this->response([
                'status' => 'success',
                'message' => 'data dihapus',
            ], RestController::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'delete gagal',
            ], RestController::HTTP_INTERNAL_ERROR);
        }
    }
}
